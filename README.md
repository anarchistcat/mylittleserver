# My Little Server

## Description
This is a repo to install my own server. Gook luck to understand everything.
BTW : this is compatible only with Debian only because i don't have any other king of Linux server

## Badges
One day, maybe...

## Installation
* Clone the repository
* Install Ansible on your own computer
* Configure SSH :
  * (not mandatory but recommanded) Generate a new SSH Key dedicated to this server
  * Add your ssh key to the SSH user used for your server `ssh-copy-id -i ~/.ssh/key.pub user@ip`
  * Update `ansible.cfg` lines 220 and 231 with your own information
* Update inventory file with your own server information

## Usage
Debian servers only.

* Deploy base :
```shell
ansible-playbook playbooks/base_install.yml --diff --become --ask-become-pass
```

* Deploy K3S :
```shell
ansible-playbook playbooks/k3s.yml --diff --become --ask-become-pass
```

## Support
If you need help, please use the repo issues.

## Contributing
Not now. Sorry. Maybe one day.

## License
For open source projects, say how it is licensed.

## Project status
Still alive but slowly.

### TODO
- [x] Paramétrage SSH pour Ansible
- [x] Ansible fail2ban, portsentry, unattended upgrade
- [x] K3S
- [ ] Ajout conteneurs :
	- [ ] Transmission
	- [ ] Blog Hugo
	- [ ] Synchro Joplin
- [ ] Lets Encrypt ?
- [ ] Enregistrements DNS ?
- [ ] Monit pour blog + Joplin
- [ ] MOTD
- [ ] base_config :
  - [ ] ZSH configuration
  - [ ] Trouver pourquoi cowsay et fortune marchent pas
  - [ ] Alias ll + neovim
  - [ ] Configurer Portsentry en mode blocage
  - [ ] Fix listchanges.conf to send emails to gmail
  - [ ] Add systemctl config pour untattended-upgrade
- [x] Warning erreurs :
```
[WARNING]: Skipping callback plugin 'profile_tasks.new', unable to load
[WARNING]: Skipping callback plugin 'timer.new', unable to load
[WARNING]: Skipping callback plugin 'profile_roles.new', unable to load
```
- [ ] Fix apt_key deprecated : [here](https://www.jeffgeerling.com/blog/2022/aptkey-deprecated-debianubuntu-how-fix-ansible)
